﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.EntityContracts;
using UnityEngine;

namespace Assets.Scripts.EntityHandlers
{
    public class HeroHandler : IHeroContract
    {

        [SerializeField] private int _hitPoints;
        [SerializeField] private int _chi;
        [SerializeField] private int _speed;

        public int GetHealth(int hitPoints)
        {
            throw new NotImplementedException();
        }

        public int GetChi(int chi)
        {
            throw new NotImplementedException();
        }

        public int GetSpeed(int speed)
        {
            throw new NotImplementedException();
        }
    }
}
