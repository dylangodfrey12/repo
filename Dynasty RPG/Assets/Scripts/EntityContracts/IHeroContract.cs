﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.EntityContracts
{
    interface IHeroContract
    {
        int GetHealth(int hitPoints);
        int GetChi(int chi);
        int GetSpeed(int speed);
    }
}
